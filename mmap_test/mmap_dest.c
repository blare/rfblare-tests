#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <error.h>
#include <errno.h>
#include <string.h>

/*
       void *mmap(void *addr, size_t length, int prot, int flags,
		  int fd, off_t offset);
       int munmap(void *addr, size_t length);

       int shm_open(const char *nom, int oflag, mode_t mode);

       int shm_unlink(const char *nom);
       */

struct mapping {
	int fd;
	char* addr;
};

struct mapping map_destination_file(char* filename, unsigned long size)
{
	struct mapping dest = { .fd = -1, .addr = NULL };
	int rc;
	struct stat stat;
	dest.fd = open(filename, O_RDWR | O_CREAT, 0660);
	if (dest.fd < 0) {
		dest.fd = errno;
		perror("Opening the destination file failed");
		goto end;
	}
	rc = fstat(dest.fd, &stat);
	if (rc < 0) {
		dest.fd = errno;
		perror("Querying the destination file size failed");
		goto end;
	}
	if (stat.st_size < (long) size) {
		rc = ftruncate(dest.fd, (long) size);
		if (rc < 0) {
			dest.fd = errno;
			perror("Resizing the destination file failed");
			goto end;
		}
	}

	dest.addr = (char*) mmap(NULL, size, PROT_WRITE, MAP_SHARED, dest.fd, 0);
	if (!dest.addr) {
		dest.fd = errno;
		perror("Mapping the destination file failed");
		goto end;
	}
	printf("Destination file mapped");

end:
	return dest;
}

struct mapping map_shared_memory(char* name, unsigned long size)
{
	struct mapping shm = { .fd = -1, .addr = NULL };
	int rc;
	shm.fd = shm_open(name, O_RDWR | O_CREAT, 0660);
	if (shm.fd < 0) {
		shm.fd = errno;
		perror("Opening the shared memory failed");
		goto end;
	}
	rc = ftruncate(shm.fd, size);
	if (rc < 0) {
		shm.fd = errno;
		perror("Resizing the shared memory failed");
		goto end;
	}

	shm.addr = (char*) mmap(NULL, size, PROT_READ, MAP_PRIVATE, shm.fd, 0);
	if (!shm.addr) {
		shm.fd = errno;
		perror("Mapping the shared memory failed");
		goto end;
	}
	printf("Shared memory mapped");

end:
	return shm;
}

void usage(int argc, char** argv, unsigned long* size)
{
	char *endptr;
	if (argc != 4) {
		printf("Usage: %s destination_file destination_file_size shared_memory_name\n", argv[0]);
		exit(EINVAL);
	}

	*size = strtoul(argv[2], &endptr, 10);
	if (*endptr != '\0') {
		printf("Usage: %s destination_file destination_file_size shared_memory_name\n", argv[0]);
		printf("Second parameter must be an integer\n");
		exit(EINVAL);
	}
}

int main(int argc, char** argv)
{
	unsigned long size;
	struct mapping dest;
	struct mapping shm;
	char c = 0;
	usage(argc, argv, &size);
	printf("This program maps the destination file called '%s' and opens the "
	       "shared memory identified by '%s'\n",argv[1],argv[3]);
	puts("Which operation do you want to do first?\n"
	     "\t1: Mapping the destination file\n"
	     "\t2: Opening the shared memory\n");
	do {
		c = getchar();
	} while (c != '1' && c != '2');
	getchar();
	if (c == '1') {
		dest = map_destination_file(argv[1], size);
		if (dest.fd < 0)
			exit(dest.fd);
		printf("Press enter to map the shared memory\n");
		getchar();
		shm = map_shared_memory(argv[3], size);
		if (shm.fd < 0)
			exit(shm.fd);
	} else {
		shm = map_shared_memory(argv[3], size);
		if (shm.fd < 0)
			exit(shm.fd);
		dest = map_destination_file(argv[1], size);
		printf("Press enter to map the destination file\n");
		getchar();
		if (dest.fd < 0)
			exit(dest.fd);
	}
	printf("Press enter to read from the shared memory and write to the destination\n");
	getchar();
	strncpy(dest.addr, shm.addr, size);
	printf("Press enter to terminate the program\n");
	getchar();
	munmap(dest.addr, size);
	munmap(shm.addr, size);
	close(dest.fd);
	close(shm.fd);
	shm_unlink(argv[3]);

	return 0;
}
