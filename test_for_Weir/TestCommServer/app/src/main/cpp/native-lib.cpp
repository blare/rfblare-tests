#include <jni.h>
#include <string>
#include <sys/stat.h>

extern "C" {

jstring
Java_com_example_lgeorget_testcommserver_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

void Java_com_example_lgeorget_testcommserver_MainActivity_createPipe(
        JNIEnv *env,
        jobject thiz,
        char path[]) {
    mkfifo(path, S_IWUSR | S_IRUSR | S_IWGRP | S_IRGRP | S_IWOTH | S_IROTH);
}

}