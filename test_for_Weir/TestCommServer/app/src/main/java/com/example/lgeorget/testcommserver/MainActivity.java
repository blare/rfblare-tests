package com.example.lgeorget.testcommserver;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.os.weir.WeirManager;

public class MainActivity extends AppCompatActivity {
    public final static String SOURCE_FILENAME = "source";
    public final static String PIPE_WELLKNOWN_FILENAME = "tuyau";
    private static final String LOG_TAG = "TestCommServer";
    private static final String TAG_NAME = "TestCommServerTag";
    private static final String WEIR_SERVICE = "weir";
    private OutputStream os;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createTag();
        String pipeFileName = null;
        try {
            pipeFileName = getTestStorageDirName(PIPE_WELLKNOWN_FILENAME);
            os = new FileOutputStream(pipeFileName);
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "Error writing " + PIPE_WELLKNOWN_FILENAME, e);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Could not fetch the sibling app data dir");
        }
    }

    private void createTag() {
		WeirManager weir = (WeirManager) getSystemService(WEIR_SERVICE);
		String tagDomains[] ={"example.org"};
		weir.createTag(TAG_NAME, true, false, null, null, tagDomains);
        weir.addTagToLabel(getPackageName(), TAG_NAME);
    }

    public void createFile(View view) {
        try {
            File sourceFile = new File(getTestStorageDirName(SOURCE_FILENAME));
            OutputStream osFile = new FileOutputStream(sourceFile);
            EditText editText = (EditText) findViewById(R.id.edit_message);
            osFile.write(editText.getText().toString().getBytes());
            Log.d(LOG_TAG, "File updated");
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error writing " + SOURCE_FILENAME, e);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Could not fetch the sibling app data dir");
        }
    }

    /** Called when the user clicks the Send button */
    public void sendFile(View view) {
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();

        try {
            File sourceFile = new File(getTestStorageDirName(SOURCE_FILENAME));
            InputStream is = new FileInputStream(sourceFile);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            //pipe.delete();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error reading " + SOURCE_FILENAME + " or writing " + PIPE_WELLKNOWN_FILENAME, e);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Could not fetch the sibling app data dir");
        }
    }

    public String getTestStorageDirName(String name) throws PackageManager.NameNotFoundException {
        // Get the directory for the user's public pictures directory
        Context otherAppContext = createPackageContext("com.example.lgeorget.testcommclient", CONTEXT_RESTRICTED);
        return otherAppContext.getFilesDir().getAbsolutePath() + "/" + name;
    }

    // Example of a call to a native method
    /*TextView tv = (TextView) findViewById(R.id.sample_text);
    tv.setText(stringFromJNI());
    */

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public static native void createPipe(char[] path);
}
