package com.example.lgeorget.testcommclient;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
    public final static String DESTINATION_FILENAME = "destination";
    public final static String PIPE_WELLKNOWN_FILENAME = "tuyau";
    private static final String LOG_TAG = "TestCommClient";
    File pipe;
    InputStream is;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            pipe = getTestStorageDir(PIPE_WELLKNOWN_FILENAME);
            is = new FileInputStream(pipe);
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "Error reading " + PIPE_WELLKNOWN_FILENAME, e);
        }
    }

    /** Called when the user clicks the Receive button */
    public void receiveFile(View view) {
        try {
  /*          String pipeFileName = getFilesDir().getAbsolutePath()
                    + "/" + PIPE_WELLKNOWN_FILENAME;*/
            is = new FileInputStream(pipe);
            File destinationFile = getTestStorageDir(DESTINATION_FILENAME);
            OutputStream os = new FileOutputStream(destinationFile);
            byte[] data = new byte[256]; // if you use is.available() here, you ruin the race condition
            int nbRead = is.read(data);
            if (nbRead != data.length) {
                Log.d(LOG_TAG, "We read less from the source than available");
            }
            TextView textView = (TextView) findViewById(R.id.textDisplay);
            textView.setText(new String(data));
            os.write(data);
            os.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error writing " + DESTINATION_FILENAME + " or reading " + PIPE_WELLKNOWN_FILENAME, e);
        }
    }

    public File getTestStorageDir(String name) {
        // Get the directory for the user's public pictures directory.
        return new File(getFilesDir(), name);
    }

}
