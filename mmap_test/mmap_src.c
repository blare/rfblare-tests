#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <error.h>
#include <errno.h>
#include <string.h>

/*
       void *mmap(void *addr, size_t length, int prot, int flags,
		  int fd, off_t offset);
       int munmap(void *addr, size_t length);

       int shm_open(const char *nom, int oflag, mode_t mode);

       int shm_unlink(const char *nom);
       */

struct mapping {
	int fd;
	char* addr;
};

struct mapping map_source_file(char* filename, unsigned long size)
{
	struct mapping src = { .fd = -1, .addr = NULL };
	src.fd = open(filename, O_RDONLY);
	if (src.fd < 0) {
		src.fd = errno;
		perror("Opening the source file failed");
		goto end;
	}

	src.addr = (char*) mmap(NULL, size, PROT_READ, MAP_PRIVATE, src.fd, 0);
	if (!src.addr) {
		src.fd = errno;
		perror("Mapping the source file failed");
		goto end;
	}
	printf("Source file mapped");

end:
	return src;
}

struct mapping map_shared_memory(char* name, unsigned long size)
{
	struct mapping shm = { .fd = -1, .addr = NULL };
	int rc;
	shm.fd = shm_open(name, O_RDWR | O_CREAT, 0660);
	if (shm.fd < 0) {
		shm.fd = errno;
		perror("Opening the shared memory failed");
		goto end;
	}
	rc = ftruncate(shm.fd, size);
	if (rc < 0) {
		shm.fd = errno;
		perror("Resizing the shared memory failed");
		goto end;
	}

	shm.addr = (char*) mmap(NULL, size, PROT_WRITE, MAP_SHARED, shm.fd, 0);
	if (shm.addr == MAP_FAILED) {
		shm.fd = errno;
		perror("Mapping the shared memory failed");
		goto end;
	}
	printf("Shared memory mapped");

end:
	return shm;
}

void usage(int argc, char** argv, unsigned long* size)
{
	char *endptr;
	if (argc != 4) {
		printf("Usage: %s source_file source_file_size shared_memory_name\n", argv[0]);
		exit(EINVAL);
	}

	*size = strtoul(argv[2], &endptr, 10);
	if (*endptr != '\0') {
		printf("Usage: %s source_file source_file_size shared_memory_name\n", argv[0]);
		printf("Second parameter must be an integer\n");
		exit(EINVAL);
	}
}


int main(int argc, char** argv)
{
	unsigned long size;
	struct mapping src;
	struct mapping shm;
	char c = 0;
	usage(argc, argv, &size);
	printf("This program maps the source file called '%s' and opens the "
	       "shared memory identified by '%s'\n",argv[1],argv[3]);
	puts("Which operation do you want to do first?\n"
	     "\t1: Mapping the source file\n"
	     "\t2: Opening the shared memory\n");
	do {
		c = getchar();
	} while (c != '1' && c != '2');
	getchar(); /* eat the end of line */
	if (c == '1') {
		src = map_source_file(argv[1], size);
		if (src.fd < 0)
			exit(src.fd);
		printf("Press enter to map the shared memory\n");
		getchar();
		shm = map_shared_memory(argv[3], size);
		if (shm.fd < 0)
			exit(shm.fd);
	} else {
		shm = map_shared_memory(argv[3], size);
		if (shm.fd < 0)
			exit(shm.fd);
		src = map_source_file(argv[1], size);
		printf("Press enter to map the source file\n");
		getchar();
		if (src.fd < 0)
			exit(src.fd);
	}
	printf("Press enter to read from the source and write to the shared memory\n");
	getchar();
	strncpy(shm.addr, src.addr, size);
	printf("Press enter to terminate the program\n");
	getchar();
	munmap(src.addr, size);
	munmap(shm.addr, size);
	close(src.fd);
	close(shm.fd);
	shm_unlink(argv[3]);

	return 0;
}
