#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define FIFO "tuyau"
#define FRAGMENT_SIZE 512
int send_file(const char *name, int size);
int recv_file(int fd, int *size);

int main(int argc, char *argv[])
{
	pid_t pid;
	int dest;
	int rc = 0;
	int already_read = 0;
	int end = 0;

	if (argc < 3) {
		fprintf(stderr, "usage: %s source destination", argv[0]);
		return EINVAL;
	}

	dest = open(argv[2], O_WRONLY | O_APPEND);
	if (dest < 0) {
		rc = errno;
		perror("Could not open destination file");
		return rc;
	}

	while (!end) {
		mkfifo(FIFO, 0600);
		pid = fork();
		if (pid == -1) {
			perror("Could not fork");
			rc = errno;
			unlink(FIFO);
			break;
		} else if (pid == 0) {
			send_file(argv[1], already_read);
			exit(0);
		} else {
			//printf("Receiving a fragment (around %i)\n", already_read);
			end = recv_file(dest, &already_read);
			wait(NULL);
		}
		unlink(FIFO);
	}
	if (end > 0)
		rc = end;

	return rc;
}

int send_file(const char *name, int already_read) {
	int source;
	int pipe;
	char buffer[FRAGMENT_SIZE];
	int size;
	int rc = 0;

	//printf("Send file: %i to %i\n", already_read,  already_read + FRAGMENT_SIZE);

	source = open(name, O_RDONLY);
	lseek(source, already_read, SEEK_SET);

	pipe = open(FIFO, O_WRONLY);
	size = read(source, buffer, FRAGMENT_SIZE);
	if (size < 0) {
		rc = errno;
		goto ret;
	}
	if (size == 0) {
		rc = -1;
		goto ret;
	}

	already_read += size;
	size = write(pipe, buffer, size);
	if (size < 0)
		rc = errno;

ret:
	close(source);
	close(pipe);
	return rc;
}

int recv_file(int fd, int *already_read) {
	char buffer[512];
	int pipe = open(FIFO, O_RDONLY);
	int size = read(pipe, buffer, FRAGMENT_SIZE);

	if (size > 0) {
		write(fd, buffer, size);
//		getchar();
		*already_read += size;
	}

	close(pipe);
	return size <= 0;
}
